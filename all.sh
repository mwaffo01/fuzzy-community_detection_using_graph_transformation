#!/bin/bash

folder=$1

for subfolder in "$folder"/*/*
do
    echo $subfolder
    python main.py $subfolder/network.dat -c $subfolder/community.dat
    wait
done
