""" Script to generate plots from overlapping community detection experiments
"""

import pickle
import matplotlib.pyplot as plt


def batch_import(files):
    """ helper method to import pickle files in batch """
    imported = []
    for f in files:
        with open(f, "rb") as file:
            imported.append(pickle.load(file))
    return imported


def get_k():
    return [10, 15, 20, 25, 30]

def get_mu():
    return [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]

def get_on():
    return [200, 300, 400, 500, 600, 700, 800]

def get_om():
    return [2, 3, 4, 5, 6, 7, 8]


def get_transform_f1_louvain(triplets):
    return [dataset[0][0][0] for dataset in triplets]

def get_transform_q_louvain(triplets):
    return [dataset[0][1][0] for dataset in triplets]

def get_transform_onmi_louvain(triplets):
    return [dataset[0][2][0] for dataset in triplets]


def get_transform_f1_infomap(triplets):
    return [dataset[0][0][1] for dataset in triplets]

def get_transform_q_infomap(triplets):
    return [dataset[0][1][1] for dataset in triplets]

def get_transform_onmi_infomap(triplets):
    return [dataset[0][2][1] for dataset in triplets]


def get_transform_f1_label_propagation(triplets):
    return [dataset[0][0][2] for dataset in triplets]

def get_transform_q_label_propagation(triplets):
    return [dataset[0][1][2] for dataset in triplets]

def get_transform_onmi_label_propagation(triplets):
    return [dataset[0][2][2] for dataset in triplets]


def get_transform_f1_sbm(triplets):
    return [dataset[0][0][3] for dataset in triplets]

def get_transform_q_sbm(triplets):
    return [dataset[0][1][3] for dataset in triplets]

def get_transform_onmi_sbm(triplets):
    return [dataset[0][2][3] for dataset in triplets]


def get_fuzzy_f1_kclique(triplets):
    return [dataset[1][0][0] for dataset in triplets]

def get_fuzzy_q_kclique(triplets):
    return [dataset[1][1][0] for dataset in triplets]

def get_fuzzy_onmi_kclique(triplets):
    return [dataset[1][2][0] for dataset in triplets]


def get_fuzzy_f1_demon(triplets):
    return [dataset[1][0][1] for dataset in triplets]

def get_fuzzy_q_demon(triplets):
    return [dataset[1][1][1] for dataset in triplets]

def get_fuzzy_onmi_demon(triplets):
    return [dataset[1][2][1] for dataset in triplets]


def get_fuzzy_f1_slpa(triplets):
    return [dataset[1][0][2] for dataset in triplets]

def get_fuzzy_q_slpa(triplets):
    return [dataset[1][1][2] for dataset in triplets]

def get_fuzzy_onmi_slpa(triplets):
    return [dataset[1][2][2] for dataset in triplets]


def get_fuzzy_f1_umstmo(triplets):
    return [dataset[1][0][3] for dataset in triplets]

def get_fuzzy_q_umstmo(triplets):
    return [dataset[1][1][3] for dataset in triplets]

def get_fuzzy_onmi_umstmo(triplets):
    return [dataset[1][2][3] for dataset in triplets]


def get_fuzzy_f1_core_expansion(triplets):
    return [dataset[1][0][4] for dataset in triplets]

def get_fuzzy_q_core_expansion(triplets):
    return [dataset[1][1][4] for dataset in triplets]

def get_fuzzy_onmi_core_expansion(triplets):
    return [dataset[1][2][4] for dataset in triplets]


def get_fuzzy_f1_osbm(triplets):
    return [dataset[1][0][5] for dataset in triplets]

def get_fuzzy_q_osbm(triplets):
    return [dataset[1][1][5] for dataset in triplets]

def get_fuzzy_onmi_osbm(triplets):
    # 0 = transform
    # 1 = fuzzy

    # 0 = f1
    # 1 = q
    # 2 = onmi

    # If transform
    # 0 = louvain
    # 1 = infomap
    # 2 = label propagation
    # 3 = SBM

    # If fuzzy:
    # 0 = kclique
    # 1 = demon
    # 2 = slpa
    # 3 = umstmo
    # 4 = core_expansion
    # 5 = overlapping SBM
    return [dataset[1][2][5] for dataset in triplets]


def plot_f1_k(k):
    plt.plot(get_k(), get_transform_f1_louvain(k), marker="s", label="N-Louvain")
    plt.plot(get_k(), get_transform_f1_infomap(k), marker="s", label="N-Infomap")
    plt.plot(get_k(), get_transform_f1_label_propagation(k), marker="s", label="N-Label_Propagation")
    plt.plot(get_k(), get_transform_f1_sbm(k), marker="s", label="N-SBM")
    plt.plot(get_k(), get_fuzzy_f1_kclique(k), marker="^", label="O-K-clique")
    plt.plot(get_k(), get_fuzzy_f1_demon(k), marker="^", label="O-Demon")
    plt.plot(get_k(), get_fuzzy_f1_slpa(k), marker="^", label="O-SLPA")
    plt.plot(get_k(), get_fuzzy_f1_umstmo(k), marker="^", label="O-UMSTMO")
    plt.plot(get_k(), get_fuzzy_f1_core_expansion(k), marker="^", label="O-Core_expansion")
    plt.plot(get_k(), get_fuzzy_f1_osbm(k), marker="^", label="O-SBM")
    plt.ylabel("F1-score")
    plt.xlabel("k")
    plt.legend()
    plt.savefig("f1_k.svg")
    plt.close()

def plot_q_k(k):
    plt.plot(get_k(), get_transform_q_louvain(k), marker="s", label="N-Louvain")
    plt.plot(get_k(), get_transform_q_infomap(k), marker="s", label="N-Infomap")
    plt.plot(get_k(), get_transform_q_label_propagation(k), marker="s", label="N-Label_Propagation")
    plt.plot(get_k(), get_transform_q_sbm(k), marker="s", label="N-SBM")
    plt.plot(get_k(), get_fuzzy_q_kclique(k), marker="^", label="O-K-clique")
    plt.plot(get_k(), get_fuzzy_q_demon(k), marker="^", label="O-Demon")
    plt.plot(get_k(), get_fuzzy_q_slpa(k), marker="^", label="O-SLPA")
    plt.plot(get_k(), get_fuzzy_q_umstmo(k), marker="^", label="O-UMSTMO")
    plt.plot(get_k(), get_fuzzy_q_core_expansion(k), marker="^", label="O-Core_expansion")
    plt.plot(get_k(), get_fuzzy_q_osbm(k), marker="^", label="O-SBM")
    plt.ylabel("Extended Modularity")
    plt.xlabel("k")
    plt.legend()
    plt.savefig("q_k.svg")
    plt.close()

def plot_onmi_k(k):
    plt.plot(get_k(), get_transform_onmi_louvain(k), marker="s", label="N-Louvain")
    plt.plot(get_k(), get_transform_onmi_infomap(k), marker="s", label="N-Infomap")
    plt.plot(get_k(), get_transform_onmi_label_propagation(k), marker="s", label="N-Label_Propagation")
    plt.plot(get_k(), get_transform_onmi_sbm(k), marker="s", label="N-SBM")
    plt.plot(get_k(), get_fuzzy_onmi_kclique(k), marker="^", label="O-K-clique")
    plt.plot(get_k(), get_fuzzy_onmi_demon(k), marker="^", label="O-Demon")
    plt.plot(get_k(), get_fuzzy_onmi_slpa(k), marker="^", label="O-SLPA")
    plt.plot(get_k(), get_fuzzy_onmi_umstmo(k), marker="^", label="O-UMSTMO")
    plt.plot(get_k(), get_fuzzy_onmi_core_expansion(k), marker="^", label="O-Core_expansion")
    plt.plot(get_k(), get_fuzzy_onmi_osbm(k), marker="^", label="O-SBM")
    plt.ylabel("ONMI")
    plt.xlabel("k")
    plt.legend()
    plt.savefig("onmi_k.svg")
    plt.close()


def plot_f1_mu(mu):
    plt.plot(get_mu(), get_transform_f1_louvain(mu), marker="s", label="N-Louvain")
    plt.plot(get_mu(), get_transform_f1_infomap(mu), marker="s", label="N-Infomap")
    plt.plot(get_mu(), get_transform_f1_label_propagation(mu), marker="s", label="N-Label_Propagation")
    plt.plot(get_mu(), get_transform_f1_sbm(mu), marker="s", label="N-SBM")
    plt.plot(get_mu(), get_fuzzy_f1_kclique(mu), marker="^", label="O-K-clique")
    plt.plot(get_mu(), get_fuzzy_f1_demon(mu), marker="^", label="O-Demon")
    plt.plot(get_mu(), get_fuzzy_f1_slpa(mu), marker="^", label="O-SLPA")
    plt.plot(get_mu(), get_fuzzy_f1_umstmo(mu), marker="^", label="O-UMSTMO")
    plt.plot(get_mu(), get_fuzzy_f1_core_expansion(mu), marker="^", label="O-Core_expansion")
    plt.plot(get_mu(), get_fuzzy_f1_osbm(mu), marker="^", label="O-SBM")
    plt.ylabel("F1-score")
    plt.xlabel("$\mu$")
    plt.legend()
    plt.savefig("f1_mu.svg")
    plt.close()

def plot_q_mu(mu):
    plt.plot(get_mu(), get_transform_q_louvain(mu), marker="s", label="N-Louvain")
    plt.plot(get_mu(), get_transform_q_infomap(mu), marker="s", label="N-Infomap")
    plt.plot(get_mu(), get_transform_q_label_propagation(mu), marker="s", label="N-Label_Propagation")
    plt.plot(get_mu(), get_transform_q_sbm(mu), marker="s", label="N-SBM")
    plt.plot(get_mu(), get_fuzzy_q_kclique(mu), marker="^", label="O-K-clique")
    plt.plot(get_mu(), get_fuzzy_q_demon(mu), marker="^", label="O-Demon")
    plt.plot(get_mu(), get_fuzzy_q_slpa(mu), marker="^", label="O-SLPA")
    plt.plot(get_mu(), get_fuzzy_q_umstmo(mu), marker="^", label="O-UMSTMO")
    plt.plot(get_mu(), get_fuzzy_q_core_expansion(mu), marker="^", label="O-Core_expansion")
    plt.plot(get_mu(), get_fuzzy_q_osbm(mu), marker="^", label="O-SBM")
    plt.ylabel("Extended Modularity")
    plt.xlabel("$\mu$")
    plt.legend()
    plt.savefig("q_mu.svg")
    plt.close()

def plot_onmi_mu(mu):
    plt.plot(get_mu(), get_transform_onmi_louvain(mu), marker="s", label="N-Louvain")
    plt.plot(get_mu(), get_transform_onmi_infomap(mu), marker="s", label="N-Infomap")
    plt.plot(get_mu(), get_transform_onmi_label_propagation(mu), marker="s", label="N-Label_Propagation")
    plt.plot(get_mu(), get_transform_onmi_sbm(mu), marker="s", label="N-SBM")
    plt.plot(get_mu(), get_fuzzy_onmi_kclique(mu), marker="^", label="O-K-clique")
    plt.plot(get_mu(), get_fuzzy_onmi_demon(mu), marker="^", label="O-Demon")
    plt.plot(get_mu(), get_fuzzy_onmi_slpa(mu), marker="^", label="O-SLPA")
    plt.plot(get_mu(), get_fuzzy_onmi_umstmo(mu), marker="^", label="O-UMSTMO")
    plt.plot(get_mu(), get_fuzzy_onmi_core_expansion(mu), marker="^", label="O-Core_expansion")
    plt.plot(get_mu(), get_fuzzy_onmi_osbm(mu), marker="^", label="O-SBM")
    plt.ylabel("ONMI")
    plt.xlabel("$\mu$")
    plt.legend()
    plt.savefig("onmi_mu.svg")
    plt.close()


def plot_f1_on(on):
    plt.plot(get_on(), get_transform_f1_louvain(on), marker="s", label="N-Louvain")
    plt.plot(get_on(), get_transform_f1_infomap(on), marker="s", label="N-Infomap")
    plt.plot(get_on(), get_transform_f1_label_propagation(on), marker="s", label="N-Label_Propagation")
    plt.plot(get_on(), get_transform_f1_sbm(on), marker="s", label="N-SBM")
    plt.plot(get_on(), get_fuzzy_f1_kclique(on), marker="^", label="O-K-clique")
    plt.plot(get_on(), get_fuzzy_f1_demon(on), marker="^", label="O-Demon")
    plt.plot(get_on(), get_fuzzy_f1_slpa(on), marker="^", label="O-SLPA")
    plt.plot(get_on(), get_fuzzy_f1_umstmo(on), marker="^", label="O-UMSTMO")
    plt.plot(get_on(), get_fuzzy_f1_core_expansion(on), marker="^", label="O-Core_expansion")
    plt.plot(get_on(), get_fuzzy_f1_osbm(on), marker="^", label="O-SBM")
    plt.ylabel("F1-score")
    plt.xlabel("$O_n$")
    plt.legend()
    plt.savefig("f1_on.svg")
    plt.close()

def plot_q_on(on):
    plt.plot(get_on(), get_transform_q_louvain(on), marker="s", label="N-Louvain")
    plt.plot(get_on(), get_transform_q_infomap(on), marker="s", label="N-Infomap")
    plt.plot(get_on(), get_transform_q_label_propagation(on), marker="s", label="N-Label_Propagation")
    plt.plot(get_on(), get_transform_q_sbm(on), marker="s", label="N-SBM")
    plt.plot(get_on(), get_fuzzy_q_kclique(on), marker="^", label="O-K-clique")
    plt.plot(get_on(), get_fuzzy_q_demon(on), marker="^", label="O-Demon")
    plt.plot(get_on(), get_fuzzy_q_slpa(on), marker="^", label="O-SLPA")
    plt.plot(get_on(), get_fuzzy_q_umstmo(on), marker="^", label="O-UMSTMO")
    plt.plot(get_on(), get_fuzzy_q_osbm(on), marker="^", label="O-SBM")
    plt.ylabel("Extended Modularity")
    plt.xlabel("$O_n$")
    plt.legend()
    plt.savefig("q_on.svg")
    plt.close()

def plot_onmi_on(on):
    plt.plot(get_on(), get_transform_onmi_louvain(on), marker="s", label="N-Louvain")
    plt.plot(get_on(), get_transform_onmi_infomap(on), marker="s", label="N-Infomap")
    plt.plot(get_on(), get_transform_onmi_label_propagation(on), marker="s", label="N-Label_Propagation")
    plt.plot(get_on(), get_transform_onmi_sbm(on), marker="s", label="N-SBM")
    plt.plot(get_on(), get_fuzzy_onmi_kclique(on), marker="^", label="O-K-clique")
    plt.plot(get_on(), get_fuzzy_onmi_demon(on), marker="^", label="O-Demon")
    plt.plot(get_on(), get_fuzzy_onmi_slpa(on), marker="^", label="O-SLPA")
    plt.plot(get_on(), get_fuzzy_onmi_umstmo(on), marker="^", label="O-UMSTMO")
    plt.plot(get_on(), get_fuzzy_onmi_core_expansion(on), marker="^", label="O-Core_expansion")
    plt.plot(get_on(), get_fuzzy_onmi_osbm(on), marker="^", label="O-SBM")
    plt.ylabel("ONMI")
    plt.xlabel("$O_n$")
    plt.legend()
    plt.savefig("onmi_on.svg")
    plt.close()


def plot_f1_om(om):
    plt.plot(get_om(), get_transform_f1_louvain(om), marker="s", label="N-Louvain")
    plt.plot(get_om(), get_transform_f1_infomap(om), marker="s", label="N-Infomap")
    plt.plot(get_om(), get_transform_f1_label_propagation(om), marker="s", label="N-Label_Propagation")
    plt.plot(get_om(), get_transform_f1_sbm(om), marker="s", label="N-SBM")
    plt.plot(get_om(), get_fuzzy_f1_kclique(om), marker="^", label="O-K-clique")
    plt.plot(get_om(), get_fuzzy_f1_demon(om), marker="^", label="O-Demon")
    plt.plot(get_om(), get_fuzzy_f1_slpa(om), marker="^", label="O-SLPA")
    plt.plot(get_om(), get_fuzzy_f1_umstmo(om), marker="^", label="O-UMSTMO")
    plt.plot(get_om(), get_fuzzy_f1_core_expansion(om), marker="^", label="O-Core_expansion")
    plt.plot(get_om(), get_fuzzy_f1_osbm(om), marker="^", label="O-SBM")
    plt.ylabel("F1-score")
    plt.xlabel("$O_m$")
    plt.legend()
    plt.savefig("f1_om.svg")
    plt.close()

def plot_q_om(om):
    plt.plot(get_om(), get_transform_q_louvain(om), marker="s", label="N-Louvain")
    plt.plot(get_om(), get_transform_q_infomap(om), marker="s", label="N-Infomap")
    plt.plot(get_om(), get_transform_q_label_propagation(om), marker="s", label="N-Label_Propagation")
    plt.plot(get_om(), get_transform_q_sbm(om), marker="s", label="N-SBM")
    plt.plot(get_om(), get_fuzzy_q_kclique(om), marker="^", label="O-K-clique")
    plt.plot(get_om(), get_fuzzy_q_demon(om), marker="^", label="O-Demon")
    plt.plot(get_om(), get_fuzzy_q_slpa(om), marker="^", label="O-SLPA")
    plt.plot(get_om(), get_fuzzy_q_umstmo(om), marker="^", label="O-UMSTMO")
    plt.plot(get_om(), get_fuzzy_q_core_expansion(om), marker="^", label="O-Core_expansion")
    plt.plot(get_om(), get_fuzzy_q_osbm(om), marker="^", label="O-SBM")
    plt.ylabel("Extended Modularity")
    plt.xlabel("$O_m$")
    plt.legend()
    plt.savefig("q_om.svg")
    plt.close()

def plot_onmi_om(om):
    plt.plot(get_om(), get_transform_onmi_louvain(om), marker="s", label="N-Louvain")
    plt.plot(get_om(), get_transform_onmi_infomap(om), marker="s", label="N-Infomap")
    plt.plot(get_om(), get_transform_onmi_label_propagation(om), marker="s", label="N-Label_Propagation")
    plt.plot(get_om(), get_transform_onmi_sbm(om), marker="s", label="N-SBM")
    plt.plot(get_om(), get_fuzzy_onmi_kclique(om), marker="^", label="O-K-clique")
    plt.plot(get_om(), get_fuzzy_onmi_demon(om), marker="^", label="O-Demon")
    plt.plot(get_om(), get_fuzzy_onmi_slpa(om), marker="^", label="O-SLPA")
    plt.plot(get_om(), get_fuzzy_onmi_umstmo(om), marker="^", label="O-UMSTMO")
    plt.plot(get_om(), get_fuzzy_onmi_core_expansion(om), marker="^", label="O-Core_expansion")
    plt.plot(get_om(), get_fuzzy_onmi_osbm(om), marker="^", label="O-SBM")
    plt.ylabel("ONMI")
    plt.xlabel("$O_m$")
    plt.legend()
    plt.savefig("onmi_om.svg")
    plt.close()


if __name__ == "__main__":
    # k = list of pairs containing results for (transform, fuzzy) algorithms
    # k[0] = list of triplets (f1s, qs, onmis) for lfr graph with k varying,
    # for transform algorithms
    # f1s = list of f1-scores for different algorithms
    k = batch_import(["k10", "k15", "k20", "k25", "k30"])
    mu = batch_import(["mu01", "mu02", "mu03", "mu04", "mu05", "mu06", "mu07",
                       "mu08"])
    on = batch_import(["om2", "om3", "om4", "om5", "om6", "om7", "om8"])
    om = batch_import(["on2", "on3", "on4", "on5", "on6", "on7", "on8"])

    plot_f1_k(k)
    plot_q_k(k)
    plot_onmi_k(k)

    plot_f1_mu(mu)
    plot_q_mu(mu)
    plot_onmi_mu(mu)

    plot_f1_on(on)
    plot_q_on(on)
    plot_onmi_on(on)

    plot_f1_om(om)
    plot_q_om(om)
    plot_onmi_om(om)
