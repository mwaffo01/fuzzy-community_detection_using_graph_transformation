import argparse

"""
This converts a ground truth formatted like

node_1 node_2 node_3 ... node_k

One line per community

Into:

node_1 community_i
node_2 community_i
node_3 community_j
"""

if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(prog="convert_gt.py",
                                     description="""Computes e computed.""",
                                     # epilog=""
                                     )
    PARSER.add_argument("ground_truth_file", help="""Path to a ground truth
                        file with all nodes of a community on the same line,
                        separated by a space""", type=str)
    ARGS = PARSER.parse_args()

    with open(ARGS.ground_truth_file) as f:
        for i, line in enumerate(f):
            for node in line.split():
                print(node, i)
