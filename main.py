import argparse
import random
import time
import pickle
# import itertools
from collections import defaultdict
import networkx as nx
import matplotlib.pyplot as plt
import community as community_louvain  # Import the community module correctly
from cdlib import algorithms, evaluation, NodeClustering
import graph_tool.all as gt


def get_example_graph():
    gm2 = nx.Graph()
    gm2.add_nodes_from(['1', '2', '3', '4', 'x', 'a', 'b', 'c'])
    gm2.add_edges_from([('1', '2'), ('1', '3'), ('1', 'x'), ('2', '3'),
                        ('3', 'x'), ('2', 'x'), ('4', '1'), ('4', '2'),
                        ('4', '3'), ('4', 'x'), ('a', 'b'), ('a', 'c'),
                        ('a', 'x'), ('b', 'c'), ('b', 'x'), ('c', 'x')])
    return gm2


def draw_example_graph(gm2):
    # Define the color for each node
    node_color = ['red' if node == 'x' else 'blue' for node in gm2]

    # Draw the graph
    plt.figure(figsize=(8, 6))
    plt.title("Original Graph Gm2")
    nx.draw(gm2, with_labels=True, node_color=node_color)
    plt.show()


def draw_graph(g):
    plt.figure(figsize=(8, 6))
    plt.title("Original Graph G7")
    nx.draw(g, with_labels=True, node_color='lightblue', font_weight='bold',
            node_size=700, font_size=15)
    plt.show()


def get_g7():
    # Define the graph G7
    g7 = nx.Graph()
    g7.add_nodes_from(['1', '2', '3', '4', '5', '6', '7'])
    g7.add_edge('1', '2')
    g7.add_edge('1', '3')
    g7.add_edge('1', '4')
    g7.add_edge('2', '3')
    g7.add_edge('2', '4')
    g7.add_edge('3', '4')
    g7.add_edge('1', '5')
    g7.add_edge('1', '6')
    g7.add_edge('1', '7')
    g7.add_edge('5', '6')
    g7.add_edge('6', '7')
    g7.add_edge('5', '7')
    g7.add_edge('8', '5')
    g7.add_edge('8', '6')
    g7.add_edge('8', '7')
    g7.add_edge('8', '1')
    return g7


def get_g8():
    # Define the graph G8
    g8 = nx.Graph()
    g8.add_nodes_from(['1', '2', '3', '4', 'a', 'b', 'c'])
    g8.add_edge('1', '2')
    g8.add_edge('1', '3')
    g8.add_edge('1', '4')
    g8.add_edge('2', '3')
    g8.add_edge('2', '4')
    g8.add_edge('3', '4')
    g8.add_edge('1', 'a')
    g8.add_edge('1', 'b')
    g8.add_edge('1', 'c')
    g8.add_edge('a', 'b')
    g8.add_edge('b', 'c')
    g8.add_edge('a', 'c')
    return g8


def get_g9():
    # Define the graph G9
    G9 = nx.Graph()

    # Add nodes for the first clique (1 to 10), ensuring nodes are strings
    G9.add_nodes_from(map(str, range(1, 11)))

    # Add nodes for the second clique (10 to 19), ensuring nodes are strings
    G9.add_nodes_from(map(str, range(10, 20)))

    G9.add_nodes_from(map(str, range(20, 30)))
    G9.add_nodes_from(map(str, range(30, 31)))

    # Add edges for the first clique (complete subgraph)
    for i in range(1, 11):
        for j in range(i + 1, 11):
            G9.add_edge(str(i), str(j))

    # Add edges for the second clique (complete subgraph)
    for i in range(10, 20):
        for j in range(i + 1, 20):
            G9.add_edge(str(i), str(j))

    for i in range(20, 30):
        for j in range(i + 1, 30):
            G9.add_edge(str(i), str(j))

    G9.add_edge(str(19), str(30))
    G9.add_edge(str(30), str(20))

    return G9


def dat_to_nx(input_file_path):
    # Create a new graph
    G7 = nx.Graph()

    # Read the edge list from the file
    # Assuming the file contains one edge per line, with nodes separated by a
    # single space
    with open(input_file_path, 'r', encoding="UTF-8") as file:
        for line in file:
            # Split the line into two nodes
            nodes = line.strip().split()
            # Add an edge to the graph
            G7.add_edge(nodes[0], nodes[1])

    # Optionally, print some information about the graph
    print("Number of nodes:", G7.number_of_nodes())
    print("Number of edges:", G7.number_of_edges())

    return G7


def decompose_graph(G):
    # Create the decomposed graph G'
    G_prime = nx.Graph()

    # Step 1: Create child nodes and add them to G'
    for node in G.nodes:
        neighbors = list(G.neighbors(node))
        for neighbor in neighbors:
            child_node = f"{node}_{neighbor}"
            G_prime.add_node(child_node)

            # Intra-parental connections: Form a clique among the child nodes
            for other_neighbor in neighbors:
                if neighbor != other_neighbor:
                    other_child_node = f"{node}_{other_neighbor}"
                    G_prime.add_edge(child_node, other_child_node)

    # Step 2: Add inter-parental connections
    for (node1, node2) in G.edges:
        for neighbor1 in G.neighbors(node1):
            for neighbor2 in G.neighbors(node2):
                # Check if there is an edge in G' already; if not, add it
                child_node1 = f"{node1}_{neighbor1}"
                child_node2 = f"{node2}_{neighbor2}"
                if not G_prime.has_edge(child_node1, child_node2):
                    G_prime.add_edge(child_node1, child_node2)

    return G_prime


def decompose_graph_with_weights(G, threshold):
    G_prime = nx.Graph()

    # Adding nodes and intra-parental edges with weights
    for node in G.nodes():
        neighbors = list(G.neighbors(node))
        # Uncomment for random order on neighbours
        # random.shuffle(neighbors)
        degree = len(neighbors)
        for i, neighbor in enumerate(neighbors):
            # enumerate starts at 0
            if i < threshold:
                child_node = f"{node}_{neighbor}"
                G_prime.add_node(child_node)

                # Form a clique among the child nodes with weighted edges
                # Ensure each pair is considered only once
                for other_neighbor in neighbors[i+1:]:
                    other_child_node = f"{node}_{other_neighbor}"
                    if not G_prime.has_edge(child_node, other_child_node):
                        G_prime.add_edge(child_node, other_child_node,
                                         weight=1/degree)
                        # print(f"Added intra-parental edge {child_node}"
                        #       f"-{other_child_node} with weight {1/degree}")

    # Adding inter-parental edges with weight 1
    for (node1, node2) in G.edges():
        for neighbor1 in G.neighbors(node1):
            for neighbor2 in G.neighbors(node2):
                child_node1 = f"{node1}_{neighbor1}"
                child_node2 = f"{node2}_{neighbor2}"
                if not G_prime.has_edge(child_node1, child_node2):
                    # Pick one weight
                    # weight = 1 / (G.degree(node1) * G.degree(node2))
                    weight = 1
                    G_prime.add_edge(child_node1, child_node2, weight=weight)
                    # print(f"Added inter-parental edge {child_node1}-"
                    #       f"{child_node2} with weight 1")

    return G_prime


def compute_fuzzy_modularity(G, communities_gn_flou):
    L = G.number_of_edges()
    A = nx.adjacency_matrix(G).todense()
    degrees = dict(G.degree())
    nodes_list = list(G.nodes())  # Convert G.nodes() to a list for indexing

    modularity = 0
    for community in communities_gn_flou:
        for (node_v, alpha_vc) in community:
            kv = degrees[node_v]
            for (node_w, alpha_wc) in community:
                kw = degrees[node_w]
                Avw = A[nodes_list.index(node_v), nodes_list.index(node_w)]
                modularity += (Avw - kv * kw / (2.0 * L)) * alpha_vc * alpha_wc

    return modularity / (2.0 * L)


def apply_louvain(G):
    # Using the Louvain algorithm to find the best partition
    partition = community_louvain.best_partition(G, randomize=True)

    # Convert the partition dictionary to a list of communities
    communities_dict = {}
    for node, comm_id in partition.items():
        if comm_id not in communities_dict:
            communities_dict[comm_id] = []
        communities_dict[comm_id].append(node)

    # Convert dictionary values (lists of nodes) to a list of communities
    communities_list = list(communities_dict.values())

    return communities_list

def apply_louvain_cdlib(G):
    # Apply the Louvain algorithm to graph G
    coms = algorithms.louvain(G)

    # Convert the NodeClustering object to a list of communities
    communities_list = [list(community) for community in coms.communities]

    return communities_list

def apply_girvan_newman(G, level=1):
    # Apply the Girvan-Newman algorithm to graph G with the specified level
    coms = algorithms.girvan_newman(G, level=level)

    # Convert the Clustering object to a list of communities
    # Each community at the specified level is a list of nodes
    communities_list = [list(community) for community in coms.communities]

    return communities_list


def apply_infomap(G):
    # Apply the Infomap algorithm to graph G
    coms = algorithms.infomap(G)

    # Convert the NodeClustering object to a list of communities
    communities_list = [list(community) for community in coms.communities]

    return communities_list


def apply_leiden(G):
    # Apply the Leiden algorithm to graph G
    coms = algorithms.leiden(G)

    # Convert the NodeClustering object to a list of communities
    communities_list = [list(community) for community in coms.communities]

    return communities_list


def apply_greedy_modularity(G):
    # Apply the Greedy Modularity community detection algorithm to graph G
    coms = algorithms.greedy_modularity(G)

    # Convert the NodeClustering object to a list of communities
    communities_list = [list(community) for community in coms.communities]

    return communities_list


def apply_label_propagation(G):
    # Apply the label propagation algorithm to graph G
    coms = algorithms.label_propagation(G)

    # Convert the NodeClustering object to a list of communities
    communities_list = [list(community) for community in coms.communities]

    return communities_list


def apply_SBM(G):
    # Apply the label propagation algorithm to graph G
    coms = algorithms.sbm_dl(G)

    # Convert the NodeClustering object to a list of communities
    communities_list = [list(community) for community in coms.communities]

    return communities_list


def transform_communities(G, communities_g_prime):
    """ TODO: Issue when nodes are not strings? """
    overlapping_communities_with_degree = []
    overlapping_communities_without_degree = []

    for community in communities_g_prime:
        community_dict = {}
        for child_node in community:
            # Extract the parent node name
            parent_node = child_node.split('_')[0]
            if parent_node in community_dict:
                community_dict[parent_node] += 1
            else:
                community_dict[parent_node] = 1

        # Convert counts to belonging degrees
        fuzzy_community = []
        crisp_community = []
        for node, count in community_dict.items():
            degree = G.degree[node]  # Degree of the node in the original graph
            belonging_degree = count / degree
            fuzzy_community.append((node, belonging_degree))
            crisp_community.append(node)

        overlapping_communities_with_degree.append(fuzzy_community)
        overlapping_communities_without_degree.append(crisp_community)

    return (overlapping_communities_with_degree,
            overlapping_communities_without_degree)









def calculate_eq(G, communities):
    """
    Optimized EQ calculation for large graphs using edge-based iteration.
    Avoids explicit adjacency matrix storage.
    """
    m = G.size(weight='weight')  # Total edge weight
    degrees = dict(G.degree(weight='weight'))  # Node degrees

    # Count how many communities each node belongs to (overlapping factor)
    O = {node: 0 for node in G.nodes()}
    for community in communities:
        for node in community:
            O[node] += 1

    EQ = 0
    for C_l in communities:
        C_l = list(C_l)  # Convert to list for indexing

        # Use a dictionary to store membership counts in this community
        community_set = set(C_l)

        # Iterate over all pairs (i, j) in the community
        for i in range(len(C_l)):
            for j in range(len(C_l)):  # This ensures all pairs (i, j) are considered
                u, v = C_l[i], C_l[j]

                if G.has_edge(u, v):
                    A_ij = G[u][v].get("weight", 1)
                else:
                    A_ij = 0

                EQ += ((A_ij - degrees[u] * degrees[v] / (2 * m)) /
                       (O[u] * O[v]))

    EQ /= (2 * m)
    return EQ



"""
def calculate_eq(G, communities):
    # total weight (or number) of edges in the graph
    m = G.size(weight='weight')
    A = nx.to_numpy_array(G)  # adjacency matrix as NumPy array
    degrees = dict(G.degree(weight='weight'))  # degree for each node
    node_list = list(G.nodes())  # list of nodes for indexing

    # Calculate O_i for each node i
    O = {node: 0 for node in G.nodes()}
    for community in communities:
        for node in community:
            O[node] += 1

    # Calculate extended modularity EQ
    EQ = 0
    for l, C_l in enumerate(communities):
        for i in C_l:
            for j in C_l:
                A_ij = A[node_list.index(i)][node_list.index(j)]
                EQ += ((A_ij - degrees[i] * degrees[j] / (2 * m)) /
                       (O[i] * O[j]))

    EQ /= (2 * m)
    return EQ
"""

def parse_community_file(filepath):
    """
    Parse the community file into a list of sets, where each set represents a
    community. Assumes lines in the file are formatted as:
        node_id  community_id1 community_id2 ...
    Separated by tabs or multiple spaces.
    """
    # Dictionary to hold community data: key is the community id, value is a
    # list of members
    communities = defaultdict(set)

    with open(filepath, 'r', encoding="UTF-8") as file:
        for line in file:
            parts = line.strip().split()  # Splits on any whitespace by default
            if len(parts) > 1:
                node = parts[0]
                memberships = parts[1:]
                for community in memberships:
                    communities[community].add(node)

    # Convert the dictionary to a list of sets
    return list(communities.values())



def read_community_file(filename):
    community_dict = {}
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split()
            node = parts[0]
            communities = parts[1:]
            for community in communities:
                if community not in community_dict:
                    community_dict[community] = []
                community_dict[community].append(node)
    # Convert dict to list of lists format
    return list(community_dict.values())



def compute_f_score(detected_communities, ground_truth_communities):
    f_scores = []

    # Function to calculate precision, recall, and F-score
    def f_score(detected_set, truth_set):
        if not detected_set or not truth_set:
            return 0
        precision = (len(detected_set.intersection(truth_set)) /
                     len(detected_set))
        recall = len(detected_set.intersection(truth_set)) / len(truth_set)
        if precision + recall == 0:
            return 0  # Avoid division by zero
        return 2 * (precision * recall) / (precision + recall)

    # For each detected community, find the best matching ground truth
    # community
    for detected in detected_communities:
        max_f = max(
            f_score(detected, truth) for truth in ground_truth_communities)
        f_scores.append(max_f)

    # Return the average F-score if there are any, otherwise 0
    return sum(f_scores) / len(f_scores) if f_scores else 0


def compute_onmi(community_name, overlapping_without_degree, G):
    """
    Compute the Overlapping Normalized Mutual Information (ONMI) between the
    provided community and a ground truth community.

    Returns:
    - float: The ONMI score between the detected communities and ground truth.
    """
    # Assume the function read_community_file is defined elsewhere
    ground_truth_communities = read_community_file(community_name)

    # Convert ground truth and your communities into NodeClustering objects
    ground_truth = NodeClustering(communities=ground_truth_communities,
                                  graph=G, method_name="Ground Truth")
    your_community = NodeClustering(communities=overlapping_without_degree,
                                    graph=G, method_name="Your Method")

    # Compute ONMI
    onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
        your_community, ground_truth)

    # Return the ONMI result
    return onmi_score.score


def evaluate_kclique_performance(G7, community_name=None, k=3):

    # Use the k-clique algorithm with the specified k value
    t1 = time.process_time()
    kclique_communities = algorithms.kclique(G7, k=k)
    t2 = time.process_time()
    print("kclique took", t2-t1)
    your_community = NodeClustering(
        communities=kclique_communities.communities,
        graph=G7, method_name=f"k-clique k={k}")

    # Convert communities into sets for metric computation
    your_community_as_set = [set(community)
                             for community in your_community.communities]

    if community_name is not None:
        # Read ground truth communities
        ground_truth_communities = read_community_file(community_name)
        ground_truth = NodeClustering(communities=ground_truth_communities,
                                      graph=G7, method_name="Ground Truth")
        ground_truth_as_set = [set(community)
                               for community in ground_truth.communities]

        # Compute F1-score
        average_f_score = compute_f_score(your_community_as_set,
                                          ground_truth_as_set)

        # Compute ONMI
        onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
            your_community, ground_truth)
        average_onmi_score = onmi_score.score
    else:
        # Ground truth is set to None when it is unknown
        ground_truth = None
        average_f_score = 0
        average_onmi_score = 0

    # Compute Overlapping Modularity (assuming calculate_eq function exists)
    average_modularity = calculate_eq(G7, your_community_as_set)

    # Give it overlapping communities with degree
    # TODO: How to get the belonging degree of each node with cdlib's algos?
    # print(compute_fuzzy_modularity(G7, your_community_as_set))

    return average_f_score, average_modularity, average_onmi_score, your_community, ground_truth


def evaluate_demon_performance(G7, N, community_name=None):

    if community_name is not None:
        # Read ground truth communities
        ground_truth_communities = read_community_file(community_name)
        ground_truth = NodeClustering(communities=ground_truth_communities,
                                      graph=G7, method_name="Ground Truth")
        ground_truth_as_set = [set(community)
                               for community in ground_truth.communities]

    f1_scores = []
    modularities = []
    onmi_scores = []

    for _ in range(N):
        exec_time = []
        t1 = time.process_time()
        demon_communities = algorithms.demon(G7, epsilon=0.25)
        t2 = time.process_time()
        exec_time.append(t2-t1)
        your_community = NodeClustering(
            communities=demon_communities.communities,
            graph=G7, method_name="DEMON")

        your_community_as_set = [set(community)
                                 for community in your_community.communities]
        # Compute Overlapping Modularity
        modularity = calculate_eq(G7, your_community_as_set)
        modularities.append(modularity)

        if community_name is not None:
            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            onmi_scores.append(onmi_score.score)

            # Compute F1 Score
            f1_score = compute_f_score(your_community_as_set, ground_truth_as_set)
            f1_scores.append(f1_score)

    # Calculate averages
    average_f1_score = sum(f1_scores) / N
    average_modularity = sum(modularities) / N
    average_onmi_score = sum(onmi_scores) / N
    print("demon took on average", sum(exec_time) / N)

    return (average_f1_score, average_modularity, average_onmi_score)


def evaluate_lpanni_performance(G7, community_name=None, N=10):

    if community_name is not None:
        # Read ground truth communities
        ground_truth_communities = read_community_file(community_name)
        ground_truth = NodeClustering(communities=ground_truth_communities,
                                      graph=G7, method_name="Ground Truth")
        ground_truth_as_set = [set(community)
                               for community in ground_truth.communities]

    f1_scores = []
    modularities = []
    onmi_scores = []

    for _ in range(N):
        exec_time = []
        t1 = time.process_time()
        lpanni_communities = algorithms.lpanni(G7)
        t2 = time.process_time()
        exec_time.append(t2-t1)
        your_community = NodeClustering(
            communities=lpanni_communities.communities,
            graph=G7, method_name="LPANNI")

        your_community_as_set = [set(community)
                                 for community in your_community.communities]
        # Compute Overlapping Modularity
        modularity = calculate_eq(G7, your_community_as_set)
        modularities.append(modularity)

        if community_name is not None:
            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            onmi_scores.append(onmi_score.score)

            # Compute F1 Score
            f1_score = compute_f_score(your_community_as_set, ground_truth_as_set)
            f1_scores.append(f1_score)

    # Calculate averages
    average_f1_score = sum(f1_scores) / N
    average_modularity = sum(modularities) / N
    average_onmi_score = sum(onmi_scores) / N
    print("lpanni took on average", sum(exec_time) / N)

    return (average_f1_score, average_modularity, average_onmi_score)


def evaluate_slpa_performance(G7, N, community_name=None):

    if community_name is not None:
        # Read ground truth communities
        ground_truth_communities = read_community_file(community_name)
        ground_truth = NodeClustering(communities=ground_truth_communities,
                                      graph=G7, method_name="Ground Truth")
        ground_truth_as_set = [set(community)
                               for community in ground_truth.communities]

    f1_scores = []
    modularities = []
    onmi_scores = []

    for _ in range(N):
        exec_time = []
        t1 = time.process_time()
        slpa_communities = algorithms.slpa(G7)
        t2 = time.process_time()
        exec_time.append(t2-t1)
        your_community = NodeClustering(
            communities=slpa_communities.communities,
            graph=G7, method_name="SLPA")

        your_community_as_set = [set(community)
                                 for community in your_community.communities]
        # Compute Overlapping Modularity
        modularity = calculate_eq(G7, your_community_as_set)
        modularities.append(modularity)

        if community_name is not None:
            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            onmi_scores.append(onmi_score.score)

            # Compute F1 Score
            f1_score = compute_f_score(your_community_as_set, ground_truth_as_set)
            f1_scores.append(f1_score)

    # Calculate averages
    average_f1_score = sum(f1_scores) / N
    average_modularity = sum(modularities) / N
    average_onmi_score = sum(onmi_scores) / N
    print("slpa took on average", sum(exec_time) / N)

    return (average_f1_score, average_modularity, average_onmi_score)


def evaluate_umstmo_performance(G7, N, community_name=None):

    if community_name is not None:
        # Read ground truth communities
        ground_truth_communities = read_community_file(community_name)
        ground_truth = NodeClustering(communities=ground_truth_communities,
                                      graph=G7, method_name="Ground Truth")
        ground_truth_as_set = [set(community)
                               for community in ground_truth.communities]

    f1_scores = []
    modularities = []
    onmi_scores = []

    for _ in range(N):
        exec_time = []
        t1 = time.process_time()
        umstmo_communities = algorithms.umstmo(G7)
        t2 = time.process_time()
        exec_time.append(t2-t1)
        your_community = NodeClustering(
            communities=umstmo_communities.communities,
            graph=G7, method_name="UMSTMO")

        your_community_as_set = [set(community)
                                 for community in your_community.communities]
        # Compute Overlapping Modularity
        modularity = calculate_eq(G7, your_community_as_set)
        modularities.append(modularity)

        if community_name is not None:
            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            onmi_scores.append(onmi_score.score)

            # Compute F1 Score
            f1_score = compute_f_score(your_community_as_set, ground_truth_as_set)
            f1_scores.append(f1_score)

    # Calculate averages
    average_f1_score = sum(f1_scores) / N
    average_modularity = sum(modularities) / N
    average_onmi_score = sum(onmi_scores) / N
    print("umstmo took on average", sum(exec_time) / N)

    return (average_f1_score, average_modularity, average_onmi_score)


def evaluate_percomvc_performance(G7, community_name=None, N=10):

    if community_name is not None:
        # Read ground truth communities
        ground_truth_communities = read_community_file(community_name)
        ground_truth = NodeClustering(communities=ground_truth_communities,
                                      graph=G7, method_name="Ground Truth")
        ground_truth_as_set = [set(community)
                               for community in ground_truth.communities]

    f1_scores = []
    modularities = []
    onmi_scores = []

    for _ in range(N):
        exec_time = []
        t1 = time.process_time()
        percomvc_communities = algorithms.percomvc(G7)
        t2 = time.process_time()
        exec_time.append(t2-t1)
        your_community = NodeClustering(
            communities=percomvc_communities.communities,
            graph=G7, method_name="PerComVC")

        your_community_as_set = [set(community)
                                 for community in your_community.communities]
        # Compute Overlapping Modularity
        modularity = calculate_eq(G7, your_community_as_set)
        modularities.append(modularity)

        if community_name is not None:
            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            onmi_scores.append(onmi_score.score)

            # Compute F1 Score
            f1_score = compute_f_score(your_community_as_set, ground_truth_as_set)
            f1_scores.append(f1_score)

    # Calculate averages
    average_f1_score = sum(f1_scores) / N
    average_modularity = sum(modularities) / N
    average_onmi_score = sum(onmi_scores) / N
    print("percomvc took on average", sum(exec_time) / N)

    return (average_f1_score, average_modularity, average_onmi_score)


def evaluate_core_expansion_performance(G7, N, community_name=None):

    if community_name is not None:
        # Read ground truth communities
        ground_truth_communities = read_community_file(community_name)
        ground_truth = NodeClustering(communities=ground_truth_communities,
                                      graph=G7, method_name="Ground Truth")
        ground_truth_as_set = [set(community)
                               for community in ground_truth.communities]

    f1_scores = []
    modularities = []
    onmi_scores = []

    for _ in range(N):
        exec_time = []
        t1 = time.process_time()
        core_expansion_communities = algorithms.core_expansion(G7)
        t2 = time.process_time()
        exec_time.append(t2-t1)
        your_community = NodeClustering(
            communities=core_expansion_communities.communities,
            graph=G7, method_name="Core Expansion")

        your_community_as_set = [set(community)
                                 for community in your_community.communities]
        # Compute Overlapping Modularity
        modularity = calculate_eq(G7, your_community_as_set)
        modularities.append(modularity)

        if community_name is not None:
            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            onmi_scores.append(onmi_score.score)

            # Compute F1 Score
            f1_score = compute_f_score(your_community_as_set, ground_truth_as_set)
            f1_scores.append(f1_score)

    # Calculate averages
    average_f1_score = sum(f1_scores) / N
    average_modularity = sum(modularities) / N
    average_onmi_score = sum(onmi_scores) / N
    print("core_expansion took on average", sum(exec_time) / N)

    return (average_f1_score, average_modularity, average_onmi_score)


def get_prop_type(value, key=None):
    """
    Performs typing and value conversion for the graph_tool PropertyMap class.
    If a key is provided, it also ensures the key is in a format that can be
    used with the PropertyMap. Returns a tuple, (type name, value, key)
    """
    if isinstance(key, str):
        # Encode the key as utf-8
        key = key.encode('utf-8', errors='replace')

    # Deal with the value
    if isinstance(value, bool):
        tname = 'bool'

    elif isinstance(value, int):
        tname = 'float'
        value = float(value)

    elif isinstance(value, float):
        tname = 'float'

    elif isinstance(value, str):
        tname = 'string'
        value = value.encode('utf-8', errors='replace')

    elif isinstance(value, dict):
        tname = 'object'

    else:
        tname = 'string'
        value = str(value)
        
    #If key is a byte value, decode it to string
    try:
        key = key.decode('utf-8')
    except AttributeError:
        pass

    return tname, value, key


def nx2gt(nxG):
    """
    Converts a networkx graph to a graph-tool graph.
    """
    # Phase 0: Create a directed or undirected graph-tool Graph
    gtG = gt.Graph(directed=nxG.is_directed())

    # Add the Graph properties as "internal properties"
    for key, value in list(nxG.graph.items()):
        # Convert the value and key into a type for graph-tool
        tname, value, key = get_prop_type(value, key)

        prop = gtG.new_graph_property(tname)  # Create the PropertyMap
        
        gtG.graph_properties[key] = prop     # Set the PropertyMap
        gtG.graph_properties[key] = value    # Set the actual value

    # Phase 1: Add the vertex and edge property maps
    # Go through all nodes and edges and add seen properties
    # Add the node properties first
    nprops = set()  # cache keys to only add properties once
    for node, data in nxG.nodes(data=True):

        # Go through all the properties if not seen and add them.
        for key, val in list(data.items()):
            if key in nprops: continue  # Skip properties already added

            # Convert the value and key into a type for graph-tool
            tname, _, key = get_prop_type(val, key)

            prop = gtG.new_vertex_property(tname)  # Create the PropertyMap
            gtG.vertex_properties[key] = prop      # Set the PropertyMap

            # Add the key to the already seen properties
            nprops.add(key)

    # Also add the node id: in NetworkX a node can be any hashable type, but
    # in graph-tool node are defined as indices. So we capture any strings
    # in a special PropertyMap called 'id' -- modify as needed!
    gtG.vertex_properties['id'] = gtG.new_vertex_property('string')

    # Add the edge properties second
    eprops = set()  # cache keys to only add properties once
    for src, dst, data in nxG.edges(data=True):

        # Go through all the edge properties if not seen and add them.
        for key, val in list(data.items()):
            if key in eprops: continue  # Skip properties already added

            # Convert the value and key into a type for graph-tool
            tname, _, key = get_prop_type(val, key)
            
            prop = gtG.new_edge_property(tname)  # Create the PropertyMap
            gtG.edge_properties[key] = prop      # Set the PropertyMap

            # Add the key to the already seen properties
            eprops.add(key)

    # Phase 2: Actually add all the nodes and vertices with their properties
    # Add the nodes
    vertices = {}  # vertex mapping for tracking edges later
    for node, data in nxG.nodes(data=True):

        # Create the vertex and annotate for our edges later
        v = gtG.add_vertex()
        vertices[node] = v

        # Set the vertex properties, not forgetting the id property
        data['id'] = str(node)
        for key, value in list(data.items()):
            gtG.vp[key][v] = value  # vp is short for vertex_properties

    # Add the edges
    for src, dst, data in nxG.edges(data=True):

        # Look up the vertex structs from our vertices mapping and add edge.
        e = gtG.add_edge(vertices[src], vertices[dst])

        # Add the edge properties
        for key, value in list(data.items()):
            gtG.ep[key][e] = value  # ep is short for edge_properties

    # Done, finally!
    return gtG, vertices


def evaluate_sbm_overlapping_performance(G7, N, community_name=None):

    # Convert networkx graph into graph_tool graph
    g, vertices_map = nx2gt(G7)

    # Build a vertices_map that matches graph_tool vertices with netwokx
    # vertices
    vertices_map_reverse = {}
    for vertex in vertices_map:
        vertices_map_reverse[vertices_map[vertex]] = vertex

    fs = []
    qs = []
    os = []
    exec_time = []

    for _ in range(N):
        t1 = time.process_time()
        # Overlapping SBM
        state = gt.minimize_blockmodel_dl(g, state=gt.OverlapBlockState)
        t2 = time.process_time()
        exec_time.append(t2-t1)
        # Extract overlapping communities
        bv, _, _, _ = state.get_overlap_blocks()

        # Build a dictionnary with b[community_id] = [list of networkx nodes]
        b = {}
        for index, node in enumerate(bv):
            if node[0] in b:
                b[node[0]].append(vertices_map_reverse[index])
            else:
                b[node[0]] = [vertices_map_reverse[index]]
        # Build a list of list of nodes
        communities = [list(a) for a in b.values()]

        # Put the communities in cdlib format so that the rest of the code can be
        # used
        your_community = NodeClustering(communities=communities, graph=G7,
                                        method_name="SBM_overlapping")

        # Convert communities into sets for metric computation
        your_community_as_set = [set(community)
                                 for community in your_community.communities]

        if community_name is not None:
            # Read ground truth communities
            ground_truth_communities = read_community_file(community_name)
            ground_truth = NodeClustering(communities=ground_truth_communities,
                                          graph=G7, method_name="Ground Truth")
            ground_truth_as_set = [set(community) for community in ground_truth.communities]

            # Compute F1-score
            average_f_score = compute_f_score(your_community_as_set,
                                              ground_truth_as_set)

            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            average_onmi_score = onmi_score.score
        else:
            average_f_score = 0
            average_onmi_score = 0

        # Compute Overlapping Modularity (assuming calculate_eq function exists)
        average_modularity = calculate_eq(G7, your_community_as_set)

        fs.append(average_f_score)
        qs.append(average_modularity)
        os.append(average_onmi_score)

    print("OSBM took", sum(exec_time)/len(exec_time))

    return sum(fs)/len(fs), sum(qs)/len(qs), sum(os)/len(os)


def onmi_example_zkc():
    zkc = nx.karate_club_graph()

    gt_zkc = defaultdict(list)
    for node in zkc.nodes():
        gt_zkc[zkc.nodes[node]['club']].append(node)

    gt_formatted = [sorted(community) for community in gt_zkc.values()]
    gt = NodeClustering(gt_formatted, zkc, method_name="zkc_gt")

    # communities = algorithms.louvain(zkc)
    communities = algorithms.leiden(zkc)

    # return evaluation.overlapping_normalized_mutual_information_MGH(
    #     louvain_communities, leiden_communities)
    return evaluation.overlapping_normalized_mutual_information_MGH(
        communities, gt)


def detect_communities_after_transform(g, threshold, n, ground_truth_file=None):
    """
    g graph
    threshold on dmax in the transformed graph
    n number of iterations the algos are averaged over
    """
    t1 = time.process_time()
    g2 = decompose_graph_with_weights(g, threshold)
    t2 = time.process_time()
    print("Transforming graph took", t2-t1)
    print("Original graph has: \t", g.number_of_nodes(), "nodes", g.number_of_edges(), "edges and max degree", max(g.degree())[1])
    print("Transformed graph has: \t", g2.number_of_nodes(), "nodes", g2.number_of_edges(), "edges and max degree", max(g2.degree())[1])

    f1s = []
    qs = []
    os = []

    # Louvain
    partitions = []
    t1 = time.process_time()
    for _ in range(n):
        partitions.append(apply_louvain_cdlib(g2))
    transformed = [transform_communities(g, partition) for partition in partitions]
    t2 = time.process_time()
    print("Louvain took", (t2-t1)/n)
    f, q, o = get_results(transformed, ground_truth_file)
    f1s.append(f)
    qs.append(q)
    os.append(o)

    # partitions.append(apply_girvan_newman(g2, level=1))
    # print("Girvan newmann done")

    # Infomap
    partitions = []
    t1 = time.process_time()
    for _ in range(n):
        partitions.append(apply_infomap(g2))
    transformed = [transform_communities(g, partition) for partition in partitions]
    t2 = time.process_time()
    print("Infomap took", (t2-t1)/n)
    f, q, o = get_results(transformed, ground_truth_file)
    f1s.append(f)
    qs.append(q)
    os.append(o)

    # partitions.append(apply_leiden(g2))

    # Label Propagation
    partitions = []
    t1 = time.process_time()
    for _ in range(n):
        partitions.append(apply_label_propagation(g2))
    transformed = [transform_communities(g, partition) for partition in partitions]
    t2 = time.process_time()
    print("Label Proppagation took", (t2-t1)/n)
    f, q, o = get_results(transformed, ground_truth_file)
    f1s.append(f)
    qs.append(q)
    os.append(o)

    # SBM
    partitions = []
    t1 = time.process_time()
    for _ in range(n):
        partitions.append(apply_SBM(g2))
    transformed = [transform_communities(g, partition) for partition in partitions]
    t2 = time.process_time()
    print("SBM took", (t2-t1)/n)
    f, q, o = get_results(transformed, ground_truth_file)
    f1s.append(f)
    qs.append(q)
    os.append(o)

    return f1s, qs, os

def get_results(transformed, ground_truth_file=None):

    f1s = []
    fuzzy_modularities = []
    onmis = []
    for partition in transformed:
        # fuzzy_modularities.append(compute_fuzzy_modularity(g, partition[0]))
        # Compute Overlapping Modularity (assuming calculate_eq() exists)
        your_community_as_set = [set(community) for community in partition[1]]
        # print(your_community_as_set)
        fuzzy_modularities.append(calculate_eq(g, your_community_as_set))
        # print(average_modularity, fuzzy_modularities[-1])
        # print("Fuzzy mod done")

    if ground_truth_file is not None:
        for partition in transformed:
            # Convert communities into sets for metric computation
            your_community_as_set = [set(community)
                                     for community in partition[1]]

            # Read ground truth communities
            ground_truth_communities = read_community_file(ground_truth_file)
            ground_truth = NodeClustering(communities=ground_truth_communities,
                                          graph=g, method_name="Ground Truth")
            ground_truth_as_set = [set(community)
                                   for community in ground_truth.communities]

            # Compute F1-score
            f1s.append(compute_f_score(your_community_as_set,
                                       ground_truth_as_set))

            your_community = NodeClustering(
                communities=partition[1], graph=g, method_name="Your Method")
            # Compute ONMI
            onmi_score = evaluation.overlapping_normalized_mutual_information_MGH(
                your_community, ground_truth)
            onmis.append(onmi_score.score)

    return sum(f1s)/len(f1s), sum(fuzzy_modularities)/len(fuzzy_modularities), sum(onmis)/len(onmis)


def detect_fuzzy_communities(G7, n, ground_truth=None):
    f1s = []
    modularities = []
    onmis = []

    if ground_truth is None:
        # Do fuzzy com detection and compare it with detect_communities(g)
        modularities.append(evaluate_kclique_performance(G7)[1])
        modularities.append(evaluate_demon_performance(G7, n)[1])
        # modularities.append(evaluate_lpanni_performance(G7)[1])
        # print("lpanni done")
        modularities.append(evaluate_slpa_performance(G7, n)[1])
        modularities.append(evaluate_umstmo_performance(G7, n)[1])
        # modularities.append(evaluate_percomvc_performance(G7)[1])
        # print("percomvc done")
        modularities.append(evaluate_core_expansion_performance(G7, n)[1])
        modularities.append(evaluate_sbm_overlapping_performance(G7, n)[1])
    else:
        f, m, o, _, _ = evaluate_kclique_performance(G7, ground_truth)
        f1s.append(f)
        modularities.append(m)
        onmis.append(o)

        f, m, o = evaluate_demon_performance(G7, n, ground_truth)
        f1s.append(f)
        modularities.append(m)
        onmis.append(o)

        # f, m, o = evaluate_lpanni_performance(G7)
        # f1s.append(f)
        # modularities.append(m)
        # onmis.append(o)

        f, m, o = evaluate_slpa_performance(G7, n, ground_truth)
        f1s.append(f)
        modularities.append(m)
        onmis.append(o)

        f, m, o = evaluate_umstmo_performance(G7, n, ground_truth)
        f1s.append(f)
        modularities.append(m)
        onmis.append(o)

        # f, m, o = evaluate_percomvc_performance(G7)
        # f1s.append(f)
        # modularities.append(m)
        # onmis.append(o)

        f, m, o = evaluate_core_expansion_performance(G7, n, ground_truth)
        f1s.append(f)
        modularities.append(m)
        onmis.append(o)

        f, m, o = evaluate_sbm_overlapping_performance(G7, n, ground_truth)
        f1s.append(f)
        modularities.append(m)
        onmis.append(o)

    return f1s, modularities, onmis


if __name__ == "__main__":
    # Set parser
    PARSER = argparse.ArgumentParser(prog="main.py",
                                     description="""Computes the fuzzy
                                     modularity of a graph given as input.
                                     Output is a triple of (F1s, Fuzzy
                                     Modularities, ONMIs). In case -c parameter
                                     is ommited, only the fuzzy modularities
                                     are computed.""",
                                     # epilog=""
                                     )
    PARSER.add_argument("graph_file", help="Path to a graph file formatted "
                        "like \"node space node\"", type=str)
    PARSER.add_argument("-c", "--communities", help="""Path to a community file
                        formatted like \"node community1, community2, [...]
                        community_k\" """,
                        type=str)
    PARSER.add_argument("-d", "--dump", help="Path to a pickle dump file",
                        type=str)
    PARSER.add_argument("-n", "--nb_iter", help="""Number of iterations of
                        non-deterministic algos""", default=10,
                        type=int)
    ARGS = PARSER.parse_args()

    g = nx.read_edgelist(ARGS.graph_file)
    transform = detect_communities_after_transform(g, g.number_of_nodes(),
                                                   ARGS.nb_iter,
                                                   ARGS.communities)
    fuzzy = detect_fuzzy_communities(g, ARGS.nb_iter, ARGS.communities)
    print(ARGS.graph_file, "transform", transform)
    print(ARGS.graph_file, "fuzzy", fuzzy)

    if ARGS.dump is not None:
        with open(ARGS.dump, 'wb') as handle:
            pickle.dump((transform, fuzzy), handle,
                        protocol=pickle.HIGHEST_PROTOCOL)

    # g2 = decompose_graph_with_weights(g)
    # partition = apply_louvain(g2)
    # transformed = transform_communities(g, partition)
    # modularity = compute_fuzzy_modularity(g, transformed[0])

    # # Compute Overlapping Modularity (assuming calculate_eq() exists)
    # your_community_as_set = [set(community) for community in transformed[1]]
    # print(your_community_as_set)
    # eq = calculate_eq(g, your_community_as_set)
