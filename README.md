# Installation

```
# graph_tools is a binary package therefore not available on pip, see
# https://graph-tool.skewed.de/installation.html
sudo apt install python3-graph-tool
# Some cdlib optional dependencies need:
sudo apt install libgmp-dev libmpfr-dev libmpc-dev
python -m venv --system-site-packages env
source env/bin/activate
pip install -r requirements.txt
```

# Run

```
source env/bin/activate
python main.py
```

# Datasets

Datasets must be formatted like:

node1 node2
node1 node3

Ground truth must be formatted like:

node1 community1
node2 community1
node3 community2

To import datasets from SNAP, use `gunzip com-youtube.all.cmty.txt.gz` then
`python convert_gt.py com-youtube.all.cmty.txt > ground_truth`

LFR graphs can be generated with the utility in the LFR directory. Follow the
readme there.

# TODO

- Add SBM https://graph-tool.skewed.de/static/doc/quickstart.html
